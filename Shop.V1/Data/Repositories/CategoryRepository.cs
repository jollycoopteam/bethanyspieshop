﻿using Shop.V1.Data.Interfaces;
using Shop.V1.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.V1.Data.Repositories
{
    public class CategoryRepository : ICategoryRepository
    {
        private readonly PieContext _pieContext;

        public CategoryRepository(PieContext appContext)
        {
            _pieContext = appContext;
        }

        public IEnumerable<Category> Categories => _pieContext.Categories;
    }
}
