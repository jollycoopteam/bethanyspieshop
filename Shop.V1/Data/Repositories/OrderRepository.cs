﻿using Shop.V1.Data.Interfaces;
using Shop.V1.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.V1.Data.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly PieContext _pieContext;
        private readonly ShoppingCart _shoppingCart;


        public OrderRepository(PieContext appDbContext, ShoppingCart shoppingCart)
        {
            _pieContext = appDbContext;
            _shoppingCart = shoppingCart;
        }


        public void CreateOrder(Order order)
        {
            order.OrderPlaced = DateTime.Now;

            _pieContext.Orders.Add(order);

            var shoppingCartItems = _shoppingCart.ShoppingCartItems;

            foreach (var shoppingCartItem in shoppingCartItems)
            {
                var orderDetail = new OrderDetail()
                {
                    Amount = shoppingCartItem.Amount,
                    PieId = shoppingCartItem.Pie.PieId,
                    OrderId = order.OrderId,
                    Price = shoppingCartItem.Pie.Price
                };

                _pieContext.OrderDetails.Add(orderDetail);
            }

            _pieContext.SaveChanges();
        }
    }
}
