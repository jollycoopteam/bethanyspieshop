﻿using Microsoft.EntityFrameworkCore;
using Shop.V1.Data.Interfaces;
using Shop.V1.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Shop.V1.Data.Repositories
{
    public class PieRepository : IPieRepository
    {
        private readonly PieContext _pieContext;

        public PieRepository(PieContext appDbContext)
        {
            _pieContext = appDbContext;
        }

        public IEnumerable<Pie> Pies => _pieContext.Pies.Include(c => c.Category);

        public IEnumerable<Pie> PiesOfTheWeek => _pieContext.Pies.Include(c => c.Category).Where(p => p.IsPieOfTheWeek);

        public Pie GetPieById(int id)
        {
            return _pieContext.Pies.FirstOrDefault(p => p.PieId == id);
        }
    }
}
